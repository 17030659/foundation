$(document).foundation();
$(document).ready(function(){

  var roverName = $('input:radio[name=rover]:checked').val();

  $('input[name=rover]').click(
    function(){
      roverName = $('input:radio[name=rover]:checked').val();
      const fecha = new Date();
      var year = fecha.getFullYear();
      var month = fecha.getMonth()+1;
      var date = fecha.getDate();

      if(date<10){
        date = '0'+date;
      }
      if(month<10){
        month = '0'+month;
      }
      var today = year+'-'+month+'-'+date;
      
      switch(roverName){
        case 'spirit':
          $('#fecha').attr({"min": '2004-05-19',"max": '2010-03-21'})
          break;
        case 'opportunity':
          $('#fecha').attr({"min": '2004-01-26',"max": '2018-06-09'})
          break;
        case 'curiosity':
          $('#fecha').attr({"min": '2012-08-06',"max": today})
          break;
        
      }
    }
  );
  


    $('#btn').click(
        function(){
          $('#cuadricula').empty();
            var fecha  = $('#fecha').val();

            
            var settings = {
                "url": "https://api.nasa.gov/mars-photos/api/v1/rovers/"+roverName+"/photos?earth_date="+fecha+"&api_key=DEMO_KEY",
                "method": "GET",
                "timeout": 0,
              };
              
              $.ajax(settings).done(function (response) {
                var fotos = response.photos;
                console.log(response);
                fotos.forEach(element => {
                  
                  var card = "<div class='cell'>"+
                              "<div class='card shadow'>"+
                                "<img src='"+element.img_src+"'class='lazy' style='max-height: 328px;'>"+
                                "<div class='card-section'>"+
                                "<h4>Camera: "+element.camera.full_name+"</h4>"+
                                  "<p>"+element.sol+" Days on Mars</p>"+
                                "</div>"+
                              "</div>"+
                            "</div>";

                  
                  $('#cuadricula').append(card);
                  
                });

               

              }).fail(function(){
                const msgError = "<div class='grid-container'>"+
                                    "<div class='callout warning'>"+
                                      "<h5>Make sure to select the date and a Rover! </h5>"+
                                    "</div>"+
                                  "</div>";

                $('#cuadricula').append(msgError);                                  
              });

              
              $('#fecha').val('');
        }
    );
}

   
   
);