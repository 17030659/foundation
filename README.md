🚀 **Consumo de API implementando estilos con Foundation**
<p align="center">
  <img src="https://get.foundation/assets/img/learn/features/svgs/code-reduction-01.svg" width="350" title="hover text">
</p>

**Instalación**

👨‍💻 Descarga utilizando el siguiente comando dentro la carpeta donde quieras almacenarlo:

    git clone https://gitlab.com/17030659/foundation.git

📦 Instalar los paquetes de node, ejecutando dentro de la carpeta del proyecto el siguiente comando:

    npm install

😎 Instalación de Sass con el siguiente comando:
    
    npm install -g sass

👀 Para compilar tu archivo css ejecuta el siguiente comando:
(Puede cambiar dependiendo el método que usaste para la instalación de Foundation)

    sass --watch node_modules/foundation-sites/scss/app.scss:css/app.css
    sass --watch direcciónArchivo/app.scss:direcciónArchivo/app.css

El archivo app.scss contiene todos lo imports con las clases que desees incluir en 
el archivo app.css, que será el resultado de la compilación del código sass.
app.scss no existe por default ya que es un archivo que tu debes crear, el nombre que le 
desees dar en realidad no importa.

👉 [Lista de imports disponibles](https://get.foundation/sites/docs/sass.html)👈

🚀 [API NASA](https://api.nasa.gov/)
